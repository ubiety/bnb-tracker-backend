import * as path from "path";
import { fileLoader, mergeTypes, mergeResolvers } from "merge-graphql-schemas";
import { GraphQLModule } from "@graphql-modules/core";

async function getSchema() {
  const types = fileLoader(path.join(__dirname, "./types"));
  const typeDefs = mergeTypes(types, { all: true });
  const resolvers = mergeResolvers(
    fileLoader(path.join(__dirname, "./resolvers"))
  );

  const { schema } = new GraphQLModule({
    typeDefs,
    resolvers
  });

  return schema;
}

export { getSchema };
