import * as express from "express";
import { ApolloServer } from "apollo-server-express";
import * as helmet from "helmet";
import * as http from "http";
import * as https from "https";
import * as fs from "fs";
import * as cookieParser from "cookie-parser";
import * as csrf from "csurf";
import { getSchema } from "./graphql";

type ServerOptions = {
  ssl: boolean;
  environment: string;
};

async function getServer(options: ServerOptions) {
  const schema = await getSchema();
  const app = express();
  const apollo = new ApolloServer({ schema, context: session => session });
  const csrfProtection = csrf({ cookie: true });

  app.use(cookieParser());
  app.use(helmet());
  await apollo.applyMiddleware({ app, cors: true });
  app.use(csrfProtection);

  let server = null;
  if (options.ssl) {
    server = https.createServer(
      {
        key: fs.readFileSync(`./.ssl/${options.environment}/server.key`),
        cert: fs.readFileSync(`./.ssl/${options.environment}/server.crt`)
      },
      app
    );
  } else {
    server = http.createServer(app);
  }
  apollo.installSubscriptionHandlers(server);

  return server;
}

export { getServer };
