import "reflect-metadata";
import { createConnection } from "typeorm";
import { entities } from "./entity";
import { getServer } from "./server";

const configurations = {
  // Note: You may need sudo to run on port 443
  production: { ssl: true, port: 443, hostname: "example.com" },
  development: { ssl: true, port: 4000, hostname: "localhost" }
};

const environment = process.env.NODE_ENV || "production";
const config = configurations[environment];

createConnection({
  type: "sqlite",
  database: "bnb.db",
  synchronize: true,
  entities
})
  .then(async () => {
    const server = await getServer({ ssl: config.ssl, environment });

    await server.listen({ port: config.port }, () => {
      console.log(
        "🚀 Server listening at ",
        `http${config.ssl ? "s" : ""}://${config.hostname}:${config.port}/graphql`
      );
    });
  })
  .catch(error => console.log(error));
